# GitLab Artifact Report

This is a small library for writing GitLab CI artifact reports based on the running job.

## Installation

```
npm install gitlab-artifact-report
```

## Functions

### getPath

Get the output path for a type of GitLab artifact report.

- reportType `string`: The report type to get the artifact report path for.

Example, given the following job definition:

```yaml
lint:
  script: ...
  artifacts:
    reports:
      codequality: gl-codequality.json
```

```js
const report = require('gitlab-artifact-report');

report.getPath('codequality');
// > 'gl-codequality.json'
```

### write

Write a buffer, string, or JSON serializable object as a GitLab artifact.

- reportType `string`: The type of the report to write to.
- report `Array` | `Blob` | `Object` | `string`: The report data to write.
- [location] `string`: The location to write the report to. This take presedence over reportType.

Example, given the following job definition:

```yaml
lint:
  script: ...
  artifacts:
    reports:
      codequality: gl-codequality.json
```

```js
const report = require('gitlab-artifact-report');

report.write('codequality', [
  {
    description: "'unused' is assigned a value but never used.",
    fingerprint: '7815696ecbf1c96e6894b779456d330e',
    location: {
      path: 'lib/index.js',
      lines: {
        begin: 42,
      },
    },
  },
]);
```
