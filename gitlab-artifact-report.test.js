const path = require('path');

let env;

beforeEach(() => {
  env = Object.assign(process.env);
  process.env.CI_CONFIG_PATH = '__fixtures__/.gitlab-ci.yml';
});

afterEach(() => {
  process.env = env;
  jest.resetModules();
});

describe('getPath', () => {
  it('should default the CI config path to .gitlab-ci.yml', () => {
    delete process.env.CI_CONFIG_PATH;
    process.env.CI_JOB_NAME = 'eslint';
    const report = require('./gitlab-artifact-report');
    expect(report.getPath('codequality')).toBe(path.resolve('gl-codequality.json'));
  });

  it('should detect the output path from the GitLab CI environment', () => {
    process.env.CI_JOB_NAME = 'string';
    const report = require('./gitlab-artifact-report');
    expect(report.getPath('codequality')).toBe(path.resolve('gl-codequality.json'));
  });

  it('should detect the output path from the GitLab CI environment if an array is used containing a single path', () => {
    process.env.CI_JOB_NAME = 'array-one';
    const report = require('./gitlab-artifact-report');
    expect(report.getPath('sast')).toBe(path.resolve('gl-sast.json'));
  });

  it('should throw if an empty array was passed', () => {
    process.env.CI_JOB_NAME = 'array-empty';
    const report = require('./gitlab-artifact-report');
    expect(() => report.getPath('dast')).toThrow(
      'Expected array-empty.artifacts.reports.dast to be one exact path, but an empty array was found.',
    );
  });

  it('should warn if an array was passed containing multiple items', () => {
    process.env.CI_JOB_NAME = 'array-multi';
    const report = require('./gitlab-artifact-report');
    jest.spyOn(process, 'emitWarning').mockImplementation();
    expect(report.getPath('dependency_scanning')).toBe(path.resolve('1.json'));
    expect(process.emitWarning).toHaveBeenCalledWith(
      'Expected array-multi.artifacts.reports.dependency_scanning to be one exact path, but found an array instead. Using the first entry.',
    );
  });

  it('should throw if an unknown type is used', () => {
    process.env.CI_JOB_NAME = 'unknown-type';
    const report = require('./gitlab-artifact-report');
    expect(() => report.getPath('performance')).toThrow(
      'Expected unknown-type.artifacts.reports.performance to be one exact path, but found 42 instead.',
    );
  });

  it('should throw if the value is not defined', () => {
    process.env.CI_JOB_NAME = 'null';
    const report = require('./gitlab-artifact-report');
    expect(() => report.getPath('performance')).toThrow(
      'Expected null.artifacts.reports.performance to be one exact path, but no value was found.',
    );
  });
});

describe('write', () => {
  let writeFile;

  beforeEach(() => {
    process.env.CI_JOB_NAME = 'string';
    writeFile = require('write');
    jest.spyOn(writeFile, 'sync').mockImplementation();
  });

  it('should be possible to write a buffer', () => {
    const report = require('./gitlab-artifact-report');
    const buffer = Buffer.alloc(0);
    report.write('codequality', buffer);
    expect(writeFile.sync).toHaveBeenCalledWith(path.resolve('gl-codequality.json'), buffer);
  });

  it('should be possible to write a string', () => {
    const report = require('./gitlab-artifact-report');
    report.write('codequality', 'test');
    expect(writeFile.sync).toHaveBeenCalledWith(path.resolve('gl-codequality.json'), 'test');
  });

  it('should be possible to write json data', () => {
    const report = require('./gitlab-artifact-report');
    report.write('codequality', {});
    expect(writeFile.sync).toHaveBeenCalledWith(path.resolve('gl-codequality.json'), '{}');
  });

  it('should be possible to data to a specific location', () => {
    const report = require('./gitlab-artifact-report');
    report.write('codequality', 'test', 'explicit-override.json');
    expect(writeFile.sync).toHaveBeenCalledWith('explicit-override.json', 'test');
  });
});
