const fs = require('fs');
const path = require('path');

const writeFile = require('write');
const yaml = require('js-yaml');

const {
  // Used as a fallback for local testing.
  CI_CONFIG_PATH = '.gitlab-ci.yml',
  CI_JOB_NAME,
  // When run outside of CI, assume the current working directory is the project path.
  CI_PROJECT_DIR = process.cwd(),
} = process.env;

/**
 * Get the output path for a type of GitLab artifact report.
 *
 * @param {string} reportType The report type to get the artifact report path for.
 */
function getPath(reportType) {
  const jobs = yaml.load(fs.readFileSync(path.join(CI_PROJECT_DIR, CI_CONFIG_PATH), 'utf-8'));
  const { artifacts } = jobs[CI_JOB_NAME];
  let location = artifacts && artifacts.reports && artifacts.reports[reportType];
  const msg = `Expected ${CI_JOB_NAME}.artifacts.reports.${reportType} to be one exact path`;
  if (!location) {
    throw new Error(`${msg}, but no value was found.`);
  }
  if (Array.isArray(location)) {
    if (location.length === 0) {
      throw new Error(`${msg}, but an empty array was found.`);
    }
    if (location.length !== 1) {
      process.emitWarning(`${msg}, but found an array instead. Using the first entry.`);
    }
    [location] = location;
  }
  if (typeof location !== 'string') {
    throw new Error(`${msg}, but found ${JSON.stringify(location)} instead.`);
  }
  return path.resolve(CI_PROJECT_DIR, location);
}

/**
 * Write a buffer, string, or JSON serializable object as a GitLab artifact.
 *
 * @param {string} reportType The type of the report to write to.
 * @param {Array | Blob | Object | string} report The report data to write.
 * @param {string} [location] The location to write the report to. This take presedence over
 * reportType.
 */
function write(reportType, report, location) {
  const data =
    report instanceof Buffer || typeof report === 'string' ? report : JSON.stringify(report);
  writeFile.sync(location || getPath(reportType), data);
}

module.exports = {
  getPath,
  write,
};
